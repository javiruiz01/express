var faker = require("faker");
var AWS = require("aws-sdk");

function createConfiguration() {
  const ENDPOINT = process.env.DYNAMO_ENDPOINT;
  let result = { region: "us-west-2" };
  if (ENDPOINT !== "") {
    result.endpoint = ENDPOINT;
  }
  return result;
}

AWS.config.update(createConfiguration());

var appRouter = function(app) {
  app.get("/", function(req, res) {
    res.status(200).send("Welcome to our RESTful API");
  });

  app.get("/user", function(req, res) {
    var data = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      username: faker.internet.userName(),
      email: faker.internet.email()
    };
    res.status(200).send(data);
  });

  app.get("/users/:num", function(req, res) {
    var users = [];
    var num = req.params.num;

    if (isFinite(num) && num > 0) {
      for (i = 0; i <= num; i++) {
        users.push({
          firstName: faker.name.firstName(),
          lastName: faker.name.lastName(),
          username: faker.internet.userName(),
          email: faker.internet.email()
        });
        res.status(200).send(users);
      }
    } else {
      res.status(400).send({ message: "Invalid number supplied" });
    }
  });

  app.get("/dynamo/create/:name", function(req, res) {
    ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08" });
    let name = req.params.name;

    var params = {
      AttributeDefinitions: [
        {
          AttributeName: "id",
          AttributeType: "S"
        }
      ],
      KeySchema: [
        {
          AttributeName: "id",
          KeyType: "HASH"
        }
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1
      },
      TableName: name,
      StreamSpecification: {
        StreamEnabled: false
      }
    };

    ddb.createTable(
      params,
      (err, data) =>
        err
          ? res.status(500).send({ error: err })
          : res.status(200).send({ data: data })
    );
  });

  app.get("/dynamo/put/:db/:name/:lastname/:id", function(req, res) {
    let ddb = new AWS.DynamoDB.DocumentClient();
    let db = req.params.db;
    let name = req.params.name;
    let lastname = req.params.lastname;
    let id = req.params.id;

    var params = {
      TableName: db,
      Item: {
        id: id,
        name: name,
        lastname: lastname
      }
    };
    ddb.put(
      params,
      (err, data) =>
        err
          ? res.status(500).send({ error: err })
          : res.status(200).send({ data: data })
    );
  });

  app.get("/dynamo/get/:table/:id", function(req, res) {
    let ddb = new AWS.DynamoDB.DocumentClient();
    let table = req.params.table;
    let id = req.params.id;

    var params = {
      TableName: table,
      Key: {
        id: id
      }
    };

    ddb.get(
      params,
      (err, data) =>
        err
          ? res.status(500).send({ error: err })
          : res.status(200).send({ data: data })
    );
  });
};

module.exports = appRouter;
